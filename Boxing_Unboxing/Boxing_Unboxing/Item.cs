﻿namespace Boxing_Unboxing
{
    public class Item
    {
        public int ID { get; }
        public string Name { get; }
        public int Quantity { get; set; }

        public Item(int id, string name, int quantity)
        {
            ID = id;
            Name = name;
            Quantity = quantity;
        }
    }
}
