﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Boxing_Unboxing
{
    class Program
    {
        public static long GenerateItemList()
        {
            long maxSize = 10000000;
            List<Item> items = new List<Item>();
            Stopwatch timer = new Stopwatch();
            timer.Start();

            for (int i = 0; i < maxSize; i++)
            {
                Item item = new Item(1, "Sword", 1);
                items.Add(item);
            }

            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        public static long GenerateObjectList()
        {
            long maxSize = 10000000;
            List<object> items = new List<object>();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i < maxSize; i++)
            {
                Item item = new Item(1, "Sword", 1);
                items.Add(item);
            }
            stopWatch.Stop();
            return stopWatch.ElapsedMilliseconds;
        }

        static void Main(string[] args)
        {
            long itemTimer = GenerateItemList();
            long objectTimer = GenerateObjectList();

            Console.WriteLine($"Using Boxing: {objectTimer} miliseconds");
            Console.WriteLine($"Not using Boxing: {itemTimer} miliseconds");

            /*
            Output:
                 Using Boxing: 1185 miliseconds
                 Not using Boxing: 1275 miliseconds
            Conclusion:
                From the output We can say that by applying the Boxing/Unboxing principle our program is sligthly faster with larger inputs.
                And for the small inputs the boxing it won't help because it will just slow our program.
             */
        }
    }
}
