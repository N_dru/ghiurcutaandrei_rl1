﻿using iQuest.BooksAndNews.Application.Publishers;

namespace iQuest.BooksAndNews.Application.Subscribers
{
    // todo: This class must be implemented.

    /// <summary>
    /// This is a subscriber that is interested to receive notification whenever a book
    /// is printed.
    ///
    /// Subscribe to the printing office and log each book that was printed.
    /// </summary>
    public class BookLover
    {
        private string Name;
        private PrintingOffice office;
        private ILog logs;
        public BookLover(string name, PrintingOffice printingOffice, ILog log)
        {
            Name = name;
            office = printingOffice;
            logs = log;
            office.BookPrinted += OnBookPrinted;
        }

        public void OnBookPrinted(object sender, MyEventArgs args)
        {
            logs.WriteInfo(Name + " got a message about a book: " + args.Message);
        }
    }
}