﻿using iQuest.BooksAndNews.Application.Publishers;

namespace iQuest.BooksAndNews.Application.Subscribers
{
    // todo: This class must be implemented.

    /// <summary>
    /// This is a subscriber that is interested to receive notification whenever news
    /// are printed.
    ///
    /// Subscribe to the printing office and log each news that was printed.
    /// </summary>
    public class NewsHunter
    {
        private string Name;
        private PrintingOffice office;
        private ILog logs;
        public NewsHunter(string name, PrintingOffice printingOffice, ILog log)
        {
            Name = name;
            office = printingOffice;
            logs = log;
            office.NewsPrinted += OnNewsPapersPrinted;
        }

        public void OnNewsPapersPrinted(object sender, MyEventArgs args)
        {
            logs.WriteInfo(Name + " got a message about a newspaper: " + args.Message);
        }
    }
}