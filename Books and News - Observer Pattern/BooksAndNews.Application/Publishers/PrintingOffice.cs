﻿using iQuest.BooksAndNews.Application.DataAccess;
using iQuest.BooksAndNews.Application.Publications;
using System;

namespace iQuest.BooksAndNews.Application.Publishers
{
    public class MyEventArgs : EventArgs
    {
        public string Message { get; set; }
        public MyEventArgs(string message)
        {
            Message = message;
        }
    }

    // todo: This class must be implemented.

    /// <summary>
    /// This is the class that will publish books and newspapers.
    /// It must offer a mechanism through which different other classes can subscribe ether
    /// to books or to newspaper.
    /// When a book or newspaper is published, the PrintingOffice must notify all the corresponding
    /// subscribers.
    /// </summary>
    /// 
    public class PrintingOffice
    {
        private IBookRepository books;
        private INewspaperRepository news;
        public EventHandler<MyEventArgs> BookPrinted;
        public EventHandler<MyEventArgs> NewsPrinted;

        public PrintingOffice(IBookRepository bookRepository, INewspaperRepository newspaperRepository, ILog log)
        {
            books = bookRepository;
            news = newspaperRepository;
        }

        public void PrintRandom(int bookCount, int newspaperCount)
        {
            while (bookCount > 0)
            {
                Book newBook = books.GetRandom();
                string message = newBook.Title + "; " + newBook.Author + "; " + newBook.Year.ToString() + ";";
                BookPrinted?.Invoke(this, new MyEventArgs(message));
                bookCount--;
            }
            while (newspaperCount > 0)
            {
                Newspaper newPaper = news.GetRandom();
                string message = newPaper.Title + "; " + newPaper.Number.ToString() + ";";
                NewsPrinted?.Invoke(this, new MyEventArgs(message));
                newspaperCount--;
            }
        }
    }
}