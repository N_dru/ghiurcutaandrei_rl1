﻿using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.DataAccessLayer;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.UseCases;
using iQuest.VendingMachine.Model;
using iQuest.VendingMachine.ExceptionsLayer;
using iQuest.VendingMachine.PaymentLayer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace VendingMachine.Tests.UseCases.BuyUseCaseTests
{
    [TestClass]
    public class ExecuteTests
    {
        private Mock<IAuthenticationService> authenticationService;
        private Mock<IBuyView> buyView;
        private Mock<IDispenserView> dispenserView;
        private Mock<IProductRepository> repository;
        private Mock<ICashPaymentView> cashView;
        private Mock<ICardPaymentView> cardView;
        private Mock<IPaymentAlgorithm> cardPayment;
        private Mock<IPaymentAlgorithm> cashPayment;
        private Mock<IPaymentUseCase> paymentUseCase;
        private BuyUseCase buyUseCase;

        [TestInitialize]
        public void TestInitialize()
        {
            buyView = new Mock<IBuyView>();
            dispenserView = new Mock<IDispenserView>();
            repository = new Mock<IProductRepository>();
            authenticationService = new Mock<IAuthenticationService>();
            cashView = new Mock<ICashPaymentView>();
            cardView = new Mock<ICardPaymentView>();
            cardPayment = new Mock<IPaymentAlgorithm>();
            cashPayment = new Mock<IPaymentAlgorithm>();
            paymentUseCase = new Mock<IPaymentUseCase>();
            buyUseCase = new BuyUseCase(authenticationService.Object, repository.Object, buyView.Object, dispenserView.Object,paymentUseCase.Object);

        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenExecuted_ThenUserIsAskedToIntroduceColumnId()
        {
            //arrange
            List<PaymentMethod> paymentMethods = new List<PaymentMethod>();
            paymentMethods.Add(new PaymentMethod(1, "Card"));
            paymentMethods.Add(new PaymentMethod(2, "Cash"));
            buyView.Setup(x => x.RequestProduct()).Returns(2);
            repository.Setup(x => x.GetByColumn(2)).Returns(new Product(2, "Fanta", 5.0, 10));
            buyView.Setup(x => x.AskForPaymentMethod(paymentMethods)).Returns(1);
            cardView.Setup(x => x.AskForCardNumber()).Returns("1111000011110000");

            //act
            buyUseCase.Execute();

            //assert
            buyView.Verify(x => x.RequestProduct(), Times.Once);
        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenExecuted_ThenTheProductIsDispensed()
        {
            //arrange
            buyView.Setup(x => x.RequestProduct()).Returns(2);
            repository.Setup(x => x.GetByColumn(2)).Returns(new Product(2, "Fanta", 5.0, 10));
            List<PaymentMethod> paymentMethods = new List<PaymentMethod>();
            paymentMethods.Add(new PaymentMethod(1, "Card"));
            paymentMethods.Add(new PaymentMethod(2, "Cash"));
            buyView.Setup(x => x.AskForPaymentMethod(paymentMethods)).Returns(1);
            cardView.Setup(x => x.AskForCardNumber()).Returns("1111000011110000");

            //act
            buyUseCase.Execute();

            //assert
            dispenserView.Verify(x => x.DispenseProduct("Fanta"), Times.Once);
        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenProductIsBought_QuantityIsDecreased()
        {
            //arrange
            Product testProduct = new Product(2, "Fanta", 5.0, 10);
            buyView.Setup(x => x.RequestProduct()).Returns(2);
            repository.Setup(x => x.GetByColumn(2)).Returns(testProduct);
            dispenserView.Setup(x => x.DispenseProduct("Fanta"));

            //act
            buyUseCase.Execute();

            //assert
            Assert.AreEqual(9, testProduct.Quantity);
        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenCanceled_ThenCancelExceptionIsThrown()
        {
            //arrange
            buyView.Setup(x => x.RequestProduct()).Throws(new CancelException());

            //assert
            Assert.ThrowsException<CancelException>(() => { buyUseCase.Execute(); });
        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenStockIsZero_ThenInsufficientExceptionIsThrown()
        {
            //arrange
            buyView.Setup(x => x.RequestProduct()).Returns(2);
            repository.Setup(x => x.GetByColumn(2)).Returns(new Product(2, "Fanta", 5.0, 0));

            //assert
            Assert.ThrowsException<InsufficientStockException>(() => { buyUseCase.Execute(); });
        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenColumnIdNotFound_ThenInvalidColumnExceptionIsThrown()
        {
            //arrange
            buyView.Setup(x => x.RequestProduct()).Returns(3);

            //assert
            Assert.ThrowsException<InvalidColumnException>(() => { buyUseCase.Execute(); });
        }

        [TestMethod]
        public void HavingABuyUseCaseInstance_WhenChosenProductIsNull_ThenInvalidColumnExceptionIsThrown()
        {
            //arrange
            Product product = null;
            buyView.Setup(x => x.RequestProduct()).Returns(2);
            repository.Setup(x => x.GetByColumn(2)).Returns(product);

            //assert
            Assert.ThrowsException<InvalidColumnException>(() => { buyUseCase.Execute(); });
        }
    }
}
