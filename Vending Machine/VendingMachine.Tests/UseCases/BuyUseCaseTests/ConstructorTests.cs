﻿using System;
using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.DataAccessLayer;
using iQuest.VendingMachine.UseCases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using iQuest.VendingMachine;
using iQuest.VendingMachine.Commands;

namespace VendingMachine.Tests.UseCases.BuyUseCaseTests
{
    [TestClass]
    public class ConstructorTests
    {
        private Mock<IAuthenticationService> authenticationService;
        private Mock<IBuyView> buyView;
        private Mock<IDispenserView> dispenserView;
        private Mock<IProductRepository> repository;
        private Mock<IPaymentUseCase> paymentUseCase;
        private Mock<IUseCaseFactory> useCaseFactory;
        private BuyUseCase buyUseCase;

        [TestInitialize]
        public void TestSetup()
        {
            buyView = new Mock<IBuyView>();
            dispenserView = new Mock<IDispenserView>();
            repository = new Mock<IProductRepository>();
            authenticationService = new Mock<IAuthenticationService>();
            paymentUseCase = new Mock<IPaymentUseCase>();
            useCaseFactory = new Mock<IUseCaseFactory>();
        }

        [TestMethod]
        public void HavingANullAuthenticationService_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new BuyUseCase(null, repository.Object, buyView.Object, dispenserView.Object, paymentUseCase.Object);
            });
        }

        [TestMethod]
        public void HavingANullRepository_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new BuyUseCase(authenticationService.Object, null, buyView.Object, dispenserView.Object, paymentUseCase.Object);
            });
        }

        [TestMethod]
        public void HavingANullBuyView_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new BuyUseCase(authenticationService.Object, repository.Object, null, dispenserView.Object, paymentUseCase.Object);
            });
        }

        [TestMethod]
        public void HavingANullDispenserView_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new BuyUseCase(authenticationService.Object, repository.Object, buyView.Object, null, paymentUseCase.Object);
            });
        }

        [TestMethod]
        public void HavingANullPaymentUseCase_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new BuyUseCase(authenticationService.Object, repository.Object, buyView.Object, dispenserView.Object, null);
            });
        }

        [TestMethod]
        public void HavingAnotherName_WhenInitializingTheUseCase()
        {
            BuyCommand buyCommand = new BuyCommand(useCaseFactory.Object, authenticationService.Object);
            Assert.IsTrue(buyCommand.Name == "buy");
        }

        [TestMethod]
        public void HavingAnotherDescription_WhenInitializingTheUseCase()
        {
            BuyCommand buyCommand = new BuyCommand(useCaseFactory.Object, authenticationService.Object);

            Assert.IsTrue(buyCommand.Description == "Buy a product");
        }
    }
}
