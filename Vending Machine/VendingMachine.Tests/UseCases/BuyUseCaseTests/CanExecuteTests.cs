using iQuest.VendingMachine;
using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace VendingMachine.Tests.Usecases.BuyUseCaseTests
{
    [TestClass]
    public class CanExecuteTests
    {
        private Mock<IAuthenticationService> authenticationService;
        private Mock<IUseCaseFactory> useCaseFactory;

        [TestInitialize]
        public void TestSetup()
        {
            authenticationService = new Mock<IAuthenticationService>();

            useCaseFactory = new Mock<IUseCaseFactory>();
        }

        [TestMethod]
        public void HavingNoAdminLoggedIn_CanExecuteIsTrue()
        {
            authenticationService.Setup(x => x.IsUserAuthenticated).Returns(false);

            BuyCommand buyUseCase = new BuyCommand(useCaseFactory.Object, authenticationService.Object);

            Assert.IsTrue(buyUseCase.CanExecute);
        }


        [TestMethod]
        public void HavingAdminLoggedIn_CanExecuteIsFalse()
        {
            authenticationService.Setup(x => x.IsUserAuthenticated).Returns(true);

            BuyCommand buyUseCase = new BuyCommand(useCaseFactory.Object, authenticationService.Object);

            Assert.IsFalse(buyUseCase.CanExecute);
        }
    }
}

