﻿using System;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.UseCases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace VendingMachine.Tests.UseCases.PaymentUseCaseTests
{
    [TestClass]
    public class ConstructorTests
    {
        private Mock<ICashPaymentView> cashView;
        private Mock<ICardPaymentView> cardView;
        private PaymentUseCase paymentUseCase;

        [TestInitialize]
        public void TestSetup()
        {
            cashView = new Mock<ICashPaymentView>();
            cardView = new Mock<ICardPaymentView>();
        }

        [TestMethod]
        public void HavingANullCardView_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new PaymentUseCase(null, cashView.Object);
            });
        }

        [TestMethod]
        public void HavingANullRepository_WhenInitializingTheUseCase_ThrowsException()
        {
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                new PaymentUseCase(cardView.Object, null);
            });
        }
    }
}
