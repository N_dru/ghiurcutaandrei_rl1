﻿using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.UseCases;
using iQuest.VendingMachine.ExceptionsLayer;
using iQuest.VendingMachine.PaymentLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace VendingMachine.Tests.UseCases.PaymentUseCaseTests
{
    [TestClass]
    public class ExecuteTests
    {
        private Mock<ICashPaymentView> cashView;
        private Mock<ICardPaymentView> cardView;
        private Mock<IPaymentAlgorithm> cardPayment;
        private Mock<IPaymentAlgorithm> cashPayment;
        private PaymentUseCase paymentUseCase;

        [TestInitialize]
        public void TestInitialize()
        {

            cashView = new Mock<ICashPaymentView>();
            cardView = new Mock<ICardPaymentView>();
            cardPayment = new Mock<IPaymentAlgorithm>();
            cashPayment = new Mock<IPaymentAlgorithm>();
            paymentUseCase = new PaymentUseCase(cardView.Object, cashView.Object);
        }

        [TestMethod]
        public void HavingAPaymentUseCaseInstance_WhenExecuted_UserIsAskedForCardNumber()
        {
            //arrange
            paymentUseCase.MethodPayment = "Card";
            cardView.Setup(x => x.AskForCardNumber()).Returns("1452033201115674");
            //act
            paymentUseCase.Execute(5.0);
            //assert
            cardView.Verify(x => x.AskForCardNumber(), Times.Once);
        }

        [TestMethod]
        public void HavingACardPaymentUseCaseInstance_WhenExecutedSuccesfully_DisplaysSuccesfullMessage()
        {
            //arrange
            paymentUseCase.MethodPayment = "Card";
            cardView.Setup(x => x.AskForCardNumber()).Returns("1452033201115674");
            //act
            paymentUseCase.Execute(5.0);
            //assert
            cardView.Verify(x => x.DisplaySuccesfullPayment(), Times.Once);
        }

        [TestMethod]
        public void HavingACardPaymentUseCaseInstance_WhenCardNumberIsInvalid_ThrowException()
        {
            //arrange
            paymentUseCase.MethodPayment = "Card";
            cardView.Setup(x => x.AskForCardNumber()).Returns("14520332011");
            //assert
            Assert.ThrowsException<InvalidCardException>(() => { paymentUseCase.Execute(5.0); });
        }

        [TestMethod]
        public void HavingACashPaymentUseCaseInstance_WhenExecuted_IntroduceAmountOfMoney()
        {
            //arrange
            paymentUseCase.MethodPayment = "Cash";
            cashView.Setup(x => x.AskForMoney()).Returns(10);
            //act
            paymentUseCase.Execute(5.0);
            //assert
            cashView.Verify(x => x.AskForMoney(), Times.Once);
        }

        [TestMethod]
        public void HavingACashPaymentUseCaseInstance_WhenInsuffiecientMoney_ThrowException()
        {
            //arrange
            paymentUseCase.MethodPayment = "Cash";
            cashView.Setup(x => x.AskForMoney()).Returns(3.5);
            //assert
            Assert.ThrowsException<InsufficientMoneyException>(() => { paymentUseCase.Execute(5.0); });
        }

        [TestMethod]
        public void HavingACashPaymentUseCaseInstance_WhenExecutedSuccesfully_DisplaySuccesfullMessage()
        {
            //arrange
            paymentUseCase.MethodPayment = "Cash";
            cashView.Setup(x => x.AskForMoney()).Returns(10);
            //act
            paymentUseCase.Execute(5.0);
            //assert
            cashView.Verify(x => x.DisplaySuccesfullPayment(), Times.Once);
        }

        [TestMethod]
        public void HavingACashPaymentUseCaseInstance_WhenExecutedSuccesfully_GiveBackChange()
        {
            //arrange
            paymentUseCase.MethodPayment = "Cash";
            cashView.Setup(x => x.AskForMoney()).Returns(10);
            //act
            paymentUseCase.Execute(5);
            //assert
            cashView.Verify(x => x.GiveBackChange(5), Times.Once);
        }
    }
}
