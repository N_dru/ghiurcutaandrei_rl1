﻿using System;

namespace iQuest.VendingMachine.ExceptionsLayer
{
    internal class CancelException : Exception
    {
        private const string DefaultMessage = "Canceled";

        public CancelException() : base(DefaultMessage)
        {
        }
    }
}
