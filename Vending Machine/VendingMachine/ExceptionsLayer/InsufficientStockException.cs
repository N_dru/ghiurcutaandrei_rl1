﻿using System;

namespace iQuest.VendingMachine.ExceptionsLayer
{
    internal class InsufficientStockException : Exception
    {
        private const string DefaultMessage = "Insuffiecient Stock";

        public InsufficientStockException() : base(DefaultMessage)
        {
        }
    }
}
