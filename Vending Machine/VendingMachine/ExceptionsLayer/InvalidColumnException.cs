﻿using System;

namespace iQuest.VendingMachine.ExceptionsLayer
{
    internal class InvalidColumnException : Exception
    {
        private const string DefaultMessage = "Invalid Column ID";

        public InvalidColumnException() : base(DefaultMessage)
        {
        }
    }
}
