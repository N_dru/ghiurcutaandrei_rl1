﻿using System;

namespace iQuest.VendingMachine.ExceptionsLayer
{
    internal class InsufficientMoneyException : Exception
    {
        private const string DefaultMessage = "Insufficient Money";

        public InsufficientMoneyException() : base(DefaultMessage)
        {
        }
    }
}
