﻿using System;
namespace iQuest.VendingMachine.ExceptionsLayer
{
    internal class InvalidCardException : Exception
    {
        private const string DefaultMessage = "Invalid Card Number";

        public InvalidCardException() : base(DefaultMessage)
        {
        }
    }
}
