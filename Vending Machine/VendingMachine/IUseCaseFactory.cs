﻿namespace iQuest.VendingMachine
{
    public interface IUseCaseFactory
    {
        T Create<T>() where T : IUseCase;
    }
}