﻿namespace iQuest.VendingMachine
{
    public interface IUseCase
    {
        void Execute();
    }
}