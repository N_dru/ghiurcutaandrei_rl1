﻿using Autofac;

namespace iQuest.VendingMachine
{
    public class UseCaseFactory : IUseCaseFactory
    {
        private readonly IComponentContext componentContext;
        public UseCaseFactory(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
        }

        public T Create<T>() where T : IUseCase
        {
            return componentContext.Resolve<T>();
        }
    }
}
