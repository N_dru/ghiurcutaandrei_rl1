﻿using System;
using System.Collections.Generic;
using iQuest.VendingMachine.PaymentLayer;
using iQuest.VendingMachine.ExceptionsLayer;

namespace iQuest.VendingMachine.PresentationLayer
{
    internal class BuyView : DisplayBase, IBuyView
    {
        public int RequestProduct()
        {
            string choice;
            Display("Type column id:", ConsoleColor.Cyan);
            choice = Console.ReadLine();
            Console.WriteLine();
            if (choice == string.Empty)
            {
                throw new CancelException();
            }
            return int.Parse(choice);
        }

        public int AskForPaymentMethod(IEnumerable<PaymentMethod> paymentMethods)
        {
            string choice;
            int length = 0;
            foreach (PaymentMethod p in paymentMethods)
            {
                DisplayLine($"{p.Id}. {p.Name}", ConsoleColor.Blue);
                length = length + 1;
            }
            DisplayLine("How do you want to pay?", ConsoleColor.Cyan);
            choice = Console.ReadLine();
            if (choice == string.Empty)
            {
                throw new CancelException();
            }
            int id = int.Parse(choice);
            if (id <= length && id > 0)
            {
                return id;
            }
            throw new InvalidColumnException();
        }
    }
}
