﻿using iQuest.VendingMachine.Commands;
using System.Collections.Generic;

namespace iQuest.VendingMachine.PresentationLayer
{
    internal interface IMainView
    {
        ICommand ChooseCommand(IEnumerable<ICommand> useCases);
        void DisplayApplicationHeader();
    }
}