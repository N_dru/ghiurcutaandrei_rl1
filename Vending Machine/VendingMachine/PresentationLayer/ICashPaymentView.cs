﻿namespace iQuest.VendingMachine.PresentationLayer
{
    public interface ICashPaymentView
    {
        double AskForMoney();
        void DisplaySuccesfullPayment();
        void GiveBackChange(double change);
    }
}