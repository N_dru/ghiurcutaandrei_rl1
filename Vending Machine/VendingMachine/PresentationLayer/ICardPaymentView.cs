﻿namespace iQuest.VendingMachine.PresentationLayer
{
    public interface ICardPaymentView
    {
        string AskForCardNumber();
        void DisplaySuccesfullPayment();
    }
}