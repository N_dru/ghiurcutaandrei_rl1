﻿using System;

namespace iQuest.VendingMachine.PresentationLayer
{
    internal class CashPaymentView : DisplayBase, ICashPaymentView
    {
        public double AskForMoney()
        {
            string amount;
            Display("Enter Amount of Cash: ", ConsoleColor.Cyan);
            amount = Console.ReadLine();
            return double.Parse(amount);
        }

        public void GiveBackChange(double change)
        {
            DisplayLine($"You've got back {change} RON", ConsoleColor.Green);
        }

        public void DisplaySuccesfullPayment()
        {
            DisplayLine("Payment Succesfull", ConsoleColor.Green);
            Console.WriteLine("");
        }
    }
}
