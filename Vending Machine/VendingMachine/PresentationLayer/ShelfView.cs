﻿using System;
using System.Collections.Generic;
using iQuest.VendingMachine.Model;

namespace iQuest.VendingMachine.PresentationLayer
{
    internal class ShelfView : DisplayBase, IShelfView
    {
        public void DisplayProducts(IEnumerable<Product> products)
        {
            bool isEmpty = true;
            if (products != null)
            {
                foreach (Product product in products)
                {
                    if (product.Quantity > 0)
                    {
                        isEmpty = false;
                        DisplayLine($"{product.ColumnId}. {product.Name}        {product.Price} RON          {product.Quantity} remaining", ConsoleColor.Green);
                    }
                }
            }
            if (isEmpty)
            {
                DisplayLine("There are no products!", ConsoleColor.Red);
            }
        }
    }
}
