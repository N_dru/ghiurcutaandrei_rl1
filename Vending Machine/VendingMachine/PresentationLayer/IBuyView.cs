﻿using iQuest.VendingMachine.PaymentLayer;
using System.Collections.Generic;
namespace iQuest.VendingMachine.PresentationLayer
{
    public interface IBuyView
    {
        int RequestProduct();
        int AskForPaymentMethod(IEnumerable<PaymentMethod> paymentMethods);
    }
}