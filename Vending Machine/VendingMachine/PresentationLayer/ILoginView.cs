﻿namespace iQuest.VendingMachine.PresentationLayer
{
    internal interface ILoginView
    {
        string AskForPassword();
    }
}