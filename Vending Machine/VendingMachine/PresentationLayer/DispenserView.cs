﻿using System;

namespace iQuest.VendingMachine.PresentationLayer
{
    internal class DispenserView : DisplayBase, IDispenserView
    {
        public void DispenseProduct(string productName)
        {
            Display($"You've got a {productName} !", ConsoleColor.DarkMagenta);
        }
    }
}