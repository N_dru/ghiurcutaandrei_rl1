﻿using System;
using System.Linq;

namespace iQuest.VendingMachine.PresentationLayer
{
    internal class CardPaymentView : DisplayBase, ICardPaymentView
    {
        public string AskForCardNumber()
        {
            string cardNumber;
            Display("Enter Card Number: ", ConsoleColor.Cyan);
            cardNumber = Console.ReadLine();
            return string.Concat(cardNumber.Where(c => !Char.IsWhiteSpace(c)));
        }

        public void DisplaySuccesfullPayment()
        {
            DisplayLine("Payment Succesfull", ConsoleColor.Green);
            Console.WriteLine("");
        }
    }
}
