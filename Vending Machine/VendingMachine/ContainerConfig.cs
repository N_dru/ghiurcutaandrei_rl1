﻿using Autofac;
using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.Commands;
using iQuest.VendingMachine.DataAccessLayer;
using iQuest.VendingMachine.PaymentLayer;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.UseCases;
using System.Linq;
using System.Reflection;

namespace iQuest.VendingMachine
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<CardPaymentView>().As<ICardPaymentView>();
            builder.RegisterType<CashPaymentView>().As<ICashPaymentView>();

            builder.RegisterType<PaymentUseCase>().As<IPaymentUseCase>();

            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>().SingleInstance();

            builder.RegisterType<DispenserView>().As<IDispenserView>();
            builder.RegisterType<BuyView>().As<IBuyView>();
            builder.RegisterType<ShelfView>().As<IShelfView>();
            builder.RegisterType<LoginView>().As<ILoginView>();
            builder.RegisterType<MainView>().As<IMainView>();

            builder.RegisterType<InMemoryRepository>().As<IProductRepository>().SingleInstance();
            builder.RegisterType<DatabaseRepository>().As<IProductRepository>().SingleInstance();

            Assembly commands = typeof(ICommand).Assembly;
            builder.RegisterAssemblyTypes(commands)
                .As<ICommand>().AsSelf();

            Assembly useCases = typeof(IUseCase).Assembly;
            builder.RegisterAssemblyTypes(useCases)
                .As<IUseCase>().AsSelf();

            builder.RegisterType<UseCaseFactory>().As<IUseCaseFactory>();

            builder.RegisterType<VendingMachineApplication>().As<IVendingMachineApplication>();


            return builder.Build();
        }
    }
}
