﻿using Autofac;

namespace iQuest.VendingMachine
{
    internal class Bootstrapper
    {

        public Bootstrapper()
        {

        }

        public void Run()
        {
            IContainer container = ContainerConfig.Configure();
            IVendingMachineApplication application = container.Resolve<IVendingMachineApplication>();
            application.Run();
        }
    }
}