﻿using System;
using iQuest.VendingMachine.PaymentLayer;
using iQuest.VendingMachine.PresentationLayer;
using System.Collections.Generic;

namespace iQuest.VendingMachine.UseCases
{
    internal class PaymentUseCase : IPaymentUseCase
    {
        private List<IPaymentAlgorithm> paymentAlgorithms = new List<IPaymentAlgorithm>();
        private readonly ICardPaymentView cardView;
        private readonly ICashPaymentView cashView;

        public string MethodPayment { get; set; }

        public PaymentUseCase(ICardPaymentView cardView, ICashPaymentView cashView)
        {
            this.cardView = cardView ?? throw new ArgumentNullException(nameof(cardView));
            this.cashView = cashView ?? throw new ArgumentNullException(nameof(cashView));
            IPaymentAlgorithm cardPayment = new CardPayment(this.cardView);
            IPaymentAlgorithm cashPayment = new CashPayment(this.cashView);
            this.paymentAlgorithms.Add(cardPayment);
            this.paymentAlgorithms.Add(cashPayment);
        }

        public void Execute(double price)
        {
            if (MethodPayment == "Card")
            {
                this.paymentAlgorithms[0].Run(price);
            }
            else if (MethodPayment == "Cash")
            {
                this.paymentAlgorithms[1].Run(price);
            }
        }
    }
}
