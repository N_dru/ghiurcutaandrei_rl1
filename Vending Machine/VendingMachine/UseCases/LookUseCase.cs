﻿using System.Collections.Generic;
using iQuest.VendingMachine.DataAccessLayer;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.Model;

namespace iQuest.VendingMachine.UseCases
{
    internal class LookUseCase : IUseCase
    {
        private readonly IProductRepository productRepository;
        private readonly IShelfView shelfView;

        public LookUseCase(IProductRepository repository, IShelfView view)
        {
            this.productRepository = repository;
            this.shelfView = view;
        }

        public void Execute()
        {
            IEnumerable<Product> products = productRepository.GetAll();
            shelfView.DisplayProducts(products);
        }
    }
}
