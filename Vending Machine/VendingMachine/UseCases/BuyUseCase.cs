﻿using iQuest.VendingMachine.DataAccessLayer;
using iQuest.VendingMachine.Model;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.ExceptionsLayer;
using iQuest.VendingMachine.PaymentLayer;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System;

[assembly: InternalsVisibleTo("VendingMachine.Tests")]

namespace iQuest.VendingMachine.UseCases
{
    internal class BuyUseCase : IUseCase
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IProductRepository productRepository;
        private readonly IBuyView buyView;
        private readonly IDispenserView dispenserView;
        private readonly IPaymentUseCase paymentUseCase;
        private List<PaymentMethod> paymentMethods;

        public BuyUseCase(IAuthenticationService authentication, IProductRepository repository, IBuyView buyView, IDispenserView dispenserView, IPaymentUseCase paymentUseCase)
        {
            this.authenticationService = authentication ?? throw new ArgumentNullException(nameof(authenticationService));
            this.productRepository = repository ?? throw new ArgumentNullException(nameof(productRepository));
            this.buyView = buyView ?? throw new ArgumentNullException(nameof(buyView));
            this.dispenserView = dispenserView ?? throw new ArgumentNullException(nameof(dispenserView));
            this.paymentUseCase = paymentUseCase ?? throw new ArgumentNullException(nameof(paymentUseCase));
            paymentMethods = new List<PaymentMethod> {
                new PaymentMethod(1, "Card"),
                new PaymentMethod(2, "Cash")
            };
        }

        public void Execute()
        {
            int column = buyView.RequestProduct();
            Product chosenProduct = productRepository.GetByColumn(column);
            if (chosenProduct == null)
            {
                throw new InvalidColumnException();
            }
            if (chosenProduct.Quantity == 0)
            {
                throw new InsufficientStockException();
            }
            int paymentId = buyView.AskForPaymentMethod(paymentMethods);
            if (paymentId == 1)
            {
                paymentUseCase.MethodPayment = "Card";
            }
            if (paymentId == 2)
            {
                paymentUseCase.MethodPayment = "Cash";
            }
            paymentUseCase.Execute(chosenProduct.Price);
            chosenProduct.Quantity--;
            dispenserView.DispenseProduct(chosenProduct.Name);
        }
    }
}
