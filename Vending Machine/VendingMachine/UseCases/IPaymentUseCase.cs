﻿namespace iQuest.VendingMachine.UseCases
{
    public interface IPaymentUseCase
    {
        string MethodPayment { get; set; }

        void Execute(double price);
    }
}