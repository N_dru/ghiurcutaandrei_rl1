﻿using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.ExceptionsLayer;

namespace iQuest.VendingMachine.PaymentLayer
{
    internal class CashPayment : IPaymentAlgorithm
    {
        public string Name => "Cash";

        private readonly ICashPaymentView cashView;

        public CashPayment(ICashPaymentView cashView)
        {
            this.cashView = cashView;
        }

        public void Run(double price)
        {
            double insertedAmount = cashView.AskForMoney();
            if (insertedAmount < 0)
            {
                throw new CancelException();
            }
            else if (insertedAmount < price)
            {
                throw new InsufficientMoneyException();
            }
            else if (insertedAmount > price)
            {
                cashView.GiveBackChange(insertedAmount - price);
            }
            cashView.DisplaySuccesfullPayment();
        }
    }
}
