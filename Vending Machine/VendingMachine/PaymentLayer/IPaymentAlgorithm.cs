﻿namespace iQuest.VendingMachine.PaymentLayer
{
    public interface IPaymentAlgorithm
    {
        public string Name { get; }
        public void Run(double price);
    }
}
