﻿using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.ExceptionsLayer;

namespace iQuest.VendingMachine.PaymentLayer
{
    internal class CardPayment : IPaymentAlgorithm
    {
        public string Name => "Card";

        private readonly ICardPaymentView cardView;

        public CardPayment(ICardPaymentView cardView)
        {
            this.cardView = cardView;
        }

        bool IsDigitsOnly(string input)
        {
            foreach (char character in input)
            {
                if (character < '0' || character > '9')
                {
                    return false;
                }
            }
            return true;
        }

        public void Run(double price)
        {
            string cardNumber = cardView.AskForCardNumber();
            if (cardNumber == string.Empty)
            {
                throw new CancelException();
            }
            else if (cardNumber.Length != 16 || !IsDigitsOnly(cardNumber))
            {
                throw new InvalidCardException();
            }
            cardView.DisplaySuccesfullPayment();
        }
    }
}
