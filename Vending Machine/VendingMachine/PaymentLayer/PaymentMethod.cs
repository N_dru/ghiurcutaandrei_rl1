﻿namespace iQuest.VendingMachine.PaymentLayer
{
    public class PaymentMethod
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public PaymentMethod(int id, string Name)
        {
            this.Id = id;
            this.Name = Name;
        }
    }
}
