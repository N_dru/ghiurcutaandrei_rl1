﻿namespace iQuest.VendingMachine.Model
{
    public class Product
    {
        public int ColumnId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }

        public Product(int newColumnId, string newName, double newPrice, int newQuantity)
        {
            this.ColumnId = newColumnId;
            this.Name = newName;
            this.Price = newPrice;
            this.Quantity = newQuantity;
        }
    }
}