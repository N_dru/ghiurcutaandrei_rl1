﻿using System.Collections.Generic;
using iQuest.VendingMachine.Model;

namespace iQuest.VendingMachine.DataAccessLayer
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
        Product GetByColumn(int column);
    }
}
