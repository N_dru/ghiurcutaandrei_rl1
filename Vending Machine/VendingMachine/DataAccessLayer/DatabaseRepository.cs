﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using iQuest.VendingMachine.Model;

namespace iQuest.VendingMachine.DataAccessLayer
{
    internal class DatabaseRepository : IProductRepository
    {
        private List<Product> products = new List<Product>();
        public DatabaseRepository()
        {
            LoadProducts();
        }

        private void LoadProducts()
        {
            using (SQLiteConnection connection = new SQLiteConnection(LoadConnectionString()))
            {
                connection.Open();
                SQLiteCommand selectCmd = connection.CreateCommand();
                selectCmd.CommandText = "SELECT * FROM Products";
                using (var reader = selectCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        double price = reader.GetDouble(2);
                        int quantity = reader.GetInt32(3);
                        Product newProduct = new Product(id, name, price, quantity);
                        products.Add(newProduct);
                    }
                }
                connection.Close();
            }
        }

        public void Update()
        {
            using (SQLiteConnection connection = new SQLiteConnection(LoadConnectionString()))
            {
                connection.Open();
                SQLiteCommand deleteCmd = connection.CreateCommand();
                deleteCmd.CommandText = "DELETE FROM Products";
                deleteCmd.ExecuteNonQuery();

                SQLiteCommand insertCmd = connection.CreateCommand();

                foreach (Product p in products)
                {
                    insertCmd.CommandText = "INSERT INTO Products(ColumnId,Name,Price,Quantity) VALUES($ColumnId,$Name,$Price,$Quantity)";
                    insertCmd.Parameters.AddWithValue("$ColumnId", p.ColumnId);
                    insertCmd.Parameters.AddWithValue("$Name", p.Name);
                    insertCmd.Parameters.AddWithValue("$Price", p.Price);
                    insertCmd.Parameters.AddWithValue("$Quantity", p.Quantity);
                    insertCmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        private string LoadConnectionString(string id = "products")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }

        public IEnumerable<Product> GetAll()
        {
            Update();
            return products;
        }

        public Product GetByColumn(int column)
        {
            foreach (Product p in products)
            {
                if (p.ColumnId == column)
                {
                    return p;
                }
            }
            return null;
        }
    }
}
