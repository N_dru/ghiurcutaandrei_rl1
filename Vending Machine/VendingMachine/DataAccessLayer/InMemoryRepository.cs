﻿using System.Collections.Generic;
using iQuest.VendingMachine.Model;
using iQuest.VendingMachine.ExceptionsLayer;

namespace iQuest.VendingMachine.DataAccessLayer
{
    internal class InMemoryRepository : IProductRepository
    {
        private List<Product> products = new List<Product>();
        public InMemoryRepository()
        {
            products.Add(new Product(1, "Coca Cola", 4.5, 15));
            products.Add(new Product(2, "Fanta", 5, 10));
            products.Add(new Product(3, "Red Bull", 7.5, 25));
        }

        public IEnumerable<Product> GetAll()
        {
            return products;
        }

        public Product GetByColumn(int column)
        {
            foreach (Product p in products)
            {
                if (p.ColumnId == column)
                {
                    return p;
                }
            }
            return null;
        }
    }
}
