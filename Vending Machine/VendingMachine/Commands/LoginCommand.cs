﻿using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.UseCases;

namespace iQuest.VendingMachine.Commands
{
    internal class LoginCommand : ICommand
    {
        readonly IUseCaseFactory useCaseFactory;
        readonly IAuthenticationService authenticationService;
        LoginUseCase loginUseCase;

        public string Name => "login";

        public string Description => "Get access to administration section.";

        public bool CanExecute => !authenticationService.IsUserAuthenticated;

        public LoginCommand(IUseCaseFactory useCaseFactory, IAuthenticationService authenticationService)
        {
            this.useCaseFactory = useCaseFactory;
            this.authenticationService = authenticationService;
        }
        public void Execute()
        {
            loginUseCase = useCaseFactory.Create<LoginUseCase>();

            loginUseCase.Execute();
        }
    }
}
