﻿using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.UseCases;

namespace iQuest.VendingMachine.Commands
{
    internal class BuyCommand : ICommand
    {
        readonly IUseCaseFactory useCaseFactory;
        readonly IAuthenticationService authenticationService;
        BuyUseCase buyUseCase;

        public string Name => "buy";

        public string Description => "Buy a product";

        public bool CanExecute => !authenticationService.IsUserAuthenticated;

        public BuyCommand(IUseCaseFactory useCaseFactory, IAuthenticationService authenticationService)
        {
            this.useCaseFactory = useCaseFactory;
            this.authenticationService = authenticationService;
        }
        public void Execute()
        {
            buyUseCase = useCaseFactory.Create<BuyUseCase>();

            buyUseCase.Execute();
        }
    }
}
