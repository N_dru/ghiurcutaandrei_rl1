﻿using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.UseCases;

namespace iQuest.VendingMachine.Commands
{
    internal class LookCommand : ICommand
    {
        readonly IUseCaseFactory useCaseFactory;
        readonly IAuthenticationService authenticationService;
        LookUseCase lookUseCase;

        public string Name => "look";

        public string Description => "Display all the products";

        public bool CanExecute => !authenticationService.IsUserAuthenticated;

        public LookCommand(IUseCaseFactory useCaseFactory, IAuthenticationService authenticationService)
        {
            this.useCaseFactory = useCaseFactory;
            this.authenticationService = authenticationService;
        }
        public void Execute()
        {
            lookUseCase = useCaseFactory.Create<LookUseCase>();

            lookUseCase.Execute();
        }
    }
}
