﻿using iQuest.VendingMachine.Authentication;
using iQuest.VendingMachine.UseCases;

namespace iQuest.VendingMachine.Commands
{
    internal class LogoutCommand : ICommand
    {
        readonly IUseCaseFactory useCaseFactory;
        readonly IAuthenticationService authenticationService;
        LogoutUseCase logoutUseCase;

        public string Name => "logout";

        public string Description => "Restrict access to administration section.";

        public bool CanExecute => authenticationService.IsUserAuthenticated;

        public LogoutCommand(IUseCaseFactory useCaseFactory, IAuthenticationService authenticationService)
        {
            this.useCaseFactory = useCaseFactory;
            this.authenticationService = authenticationService;
        }
        public void Execute()
        {
            logoutUseCase = useCaseFactory.Create<LogoutUseCase>();

            logoutUseCase.Execute();
        }
    }
}
