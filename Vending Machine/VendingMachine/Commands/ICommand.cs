﻿namespace iQuest.VendingMachine.Commands
{
    public interface ICommand
    {
        string Name { get; }

        string Description { get; }

        bool CanExecute { get; }

        void Execute();
    }
}
