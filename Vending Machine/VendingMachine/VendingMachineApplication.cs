﻿using System;
using System.Collections.Generic;
using iQuest.VendingMachine.PresentationLayer;
using iQuest.VendingMachine.ExceptionsLayer;
using System.Linq;
using iQuest.VendingMachine.Commands;

namespace iQuest.VendingMachine
{
    internal class VendingMachineApplication : IVendingMachineApplication
    {
        private readonly IEnumerable<ICommand> useCases;
        private readonly IMainView mainView;

        public VendingMachineApplication(IEnumerable<ICommand> useCases, IMainView mainView)
        {
            this.useCases = useCases ?? throw new ArgumentNullException(nameof(useCases));
            this.mainView = mainView ?? throw new ArgumentNullException(nameof(mainView));
        }

        public void Run()
        {
            mainView.DisplayApplicationHeader();

            while (true)
            {
                List<ICommand> availableUseCases = GetExecutableUseCases();

                ICommand useCase = mainView.ChooseCommand(availableUseCases);
                try
                {
                    useCase.Execute();
                }
                catch (CancelException ex)
                {
                    DisplayMessage(ex.Message, ConsoleColor.Yellow);
                }
                catch (Exception ex)
                {
                    DisplayMessage(ex.Message, ConsoleColor.Red);
                }

            }
        }

        private static void DisplayMessage(string message, ConsoleColor color)
        {
            ConsoleColor oldColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(message);
            Console.ForegroundColor = oldColor;
        }

        private List<ICommand> GetExecutableUseCases()
        {
            return useCases.Where(useCase => useCase.CanExecute == true).ToList();
        }
    }
}