﻿using iQuest.BooksAndNews.Application.Publications;

namespace iQuest.BooksAndNews.Application.Events
{
    public delegate void NewsPrintedHandler(Newspaper n);
}
