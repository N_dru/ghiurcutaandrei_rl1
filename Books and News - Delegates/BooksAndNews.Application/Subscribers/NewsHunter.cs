﻿using iQuest.BooksAndNews.Application.Publishers;
using iQuest.BooksAndNews.Application.Publications;

namespace iQuest.BooksAndNews.Application.Subscribers
{
    // todo: This class must be implemented.

    /// <summary>
    /// This is a subscriber that is interested to receive notification whenever news
    /// are printed.
    ///
    /// Subscribe to the printing office and log each news that was printed.
    /// </summary>
    public class NewsHunter
    {
        private string Name;
        private PrintingOffice office;
        private ILog logs;
        public NewsHunter(string name, PrintingOffice printingOffice, ILog log)
        {
            Name = name;
            office = printingOffice;
            logs = log;
            office.NewsPapersPrinted += HandleNewspaperPrinted;
        }

        private void HandleNewspaperPrinted(Newspaper newspaper)
        {
            string message = newspaper.Title + "; " + newspaper.Number.ToString() + ";";
            logs.WriteInfo(Name + " got a message about a newspaper: " + message);
        }
    }
}