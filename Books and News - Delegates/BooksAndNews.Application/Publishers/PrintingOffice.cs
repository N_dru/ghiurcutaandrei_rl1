﻿using iQuest.BooksAndNews.Application.DataAccess;
using iQuest.BooksAndNews.Application.Publications;
using iQuest.BooksAndNews.Application.Events;

namespace iQuest.BooksAndNews.Application.Publishers
{
    // todo: This class must be implemented.

    /// <summary>
    /// This is the class that will publish books and newspapers.
    /// It must offer a mechanism through which different other classes can subscribe ether
    /// to books or to newspaper.
    /// When a book or newspaper is published, the PrintingOffice must notify all the corresponding
    /// subscribers.
    /// </summary>
    public class PrintingOffice
    {
        private IBookRepository books;
        private INewspaperRepository news;
        public BookPrintedHandler BookPrinted;
        public NewsPrintedHandler NewsPapersPrinted;

        public PrintingOffice(IBookRepository bookRepository, INewspaperRepository newspaperRepository, ILog log)
        {
            books = bookRepository;
            news = newspaperRepository;
        }

        public void PrintRandom(int bookCount, int newspaperCount)
        {
            while (bookCount > 0)
            {
                Book newBook = books.GetRandom();
                BookPrinted?.Invoke(newBook);
                bookCount--;
            }
            while (newspaperCount > 0)
            {
                Newspaper newPaper = news.GetRandom();
                NewsPapersPrinted?.Invoke(newPaper);
                newspaperCount--;
            }
        }
    }
}