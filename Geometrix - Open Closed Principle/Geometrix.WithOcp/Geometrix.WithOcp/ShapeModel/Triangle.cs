﻿using System;

namespace iQuest.Geometrix.WithOcp.ShapeModel
{
    internal class Triangle : IShape
    {
        public double Side1 { get; set; }
        public double Side2 { get; set; }
        public double Side3 { get; set; }

        public double CalculateArea()
        {
            double semiP = (Side1 + Side2 + Side3) / 2;
            return Math.Sqrt(semiP * (semiP - Side1) * (semiP - Side2) * (semiP - Side3));
        }
    }
}
