﻿namespace HashTable
{
    public interface IHashTable<TKey, TValue>
    {
        TValue Get(TKey key);
        void Put(TKey key, TValue value);
        void Remove(TKey key);
        bool ContainsKey(TKey key);
        int Count();
    }
}
