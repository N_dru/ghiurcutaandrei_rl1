﻿using System;

namespace HashTable
{
    public class CustomHashTable<TKey, TValue> : IHashTable<TKey, TValue>
    {
        private Node<TKey, TValue>[] table;
        private int tableSize;

        public CustomHashTable(int givenSize)
        {
            this.tableSize = 0;
            table = new Node<TKey, TValue>[givenSize];
        }

        public bool ContainsKey(TKey key)
        {
            int pos = key.GetHashCode();
            Node<TKey, TValue> node = table[pos];

            while (node != null)
            {
                if (node.Key.Equals(key))
                {
                    return true;
                }
                node = node.Next;
            }
            return false;
        }

        public int Count()
        {
            return tableSize;
        }

        public TValue Get(TKey key)
        {
            int pos = key.GetHashCode();
            Node<TKey, TValue> node = table[pos];

            while (node != null)
            {
                if (node.Key.Equals(key))
                {
                    return node.Value;
                }
                node = node.Next;
            }
            throw new InvalidOperationException("Invalid Key!");
        }

        public void Put(TKey key, TValue value)
        {
            int pos = key.GetHashCode();
            Node<TKey, TValue> node = table[pos];

            if (ContainsKey(key))
            {
                throw new InvalidOperationException("Key already used!");
            }

            this.tableSize++;
            Node<TKey, TValue> newNode = new Node<TKey, TValue>() { Key = key, Value = value };
            if (node == null)
            {
                table[pos] = newNode;
                return;
            }
            else
            {
                newNode.Next = node;
                table[pos] = newNode;
                return;
            }
        }

        public void Remove(TKey key)
        {
            int pos = key.GetHashCode();
            Node<TKey, TValue> node = table[pos];
            Node<TKey, TValue> prev = null;

            if (tableSize <= 0)
            {
                throw new InvalidOperationException("Key already used!");
            }

            while (node != null && !node.Key.Equals(key))
            {
                prev = node;
                node = node.Next;
            }

            if (node == null)
            {
                throw new InvalidOperationException("Invalid Key!");
            }

            this.tableSize--;
            if (prev == null)
            {
                table[pos] = table[pos].Next;
            }
            else
            {
                prev.Next = node.Next;
                node.Next = null;
            }
        }

        public TValue this[TKey index]
        {
            get
            {
                return Get(index);
            }
            set
            {
                Put(index, value);
            }
        }
    }
}
