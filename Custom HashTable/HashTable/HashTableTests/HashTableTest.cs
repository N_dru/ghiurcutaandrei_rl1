using HashTable;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace HashTableTests
{
    [TestClass]
    public class HashTableTest
    {
        private CustomHashTable<int, int> table;

        [TestInitialize]
        public void TestSetup()
        {
            table = new CustomHashTable<int, int>(5);
        }

        [TestMethod]
        public void Get_HavingValidKey_ReturnsValueofKey()
        {
            table.Put(0, 5);

            Assert.IsTrue(table.Get(0) == 5);
        }

        [TestMethod]
        public void Get_HavingInvalidKey_ThrowsException()
        {
            table.Put(0, 5);

            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                table.Get(2);
            });
        }

        [TestMethod]
        public void Count_HavingSizeN_ReturnsSizeN()
        {
            table.Put(0, 5);

            Assert.IsTrue(table.Count() == 1);
        }

        [TestMethod]
        public void ContainsKey_HavingValidKey_ReturnsTrue()
        {
            table.Put(0, 5);

            Assert.IsTrue(table.ContainsKey(0));
        }

        [TestMethod]
        public void ContainsKey_HavingInvalidKey_ReturnsFalse()
        {
            table.Put(0, 5);

            Assert.IsFalse(table.ContainsKey(1));
        }

        [TestMethod]
        public void Put_HavingValidKeyValue_InsertInTable()
        {
            table.Put(0, 5);

            Assert.IsTrue(table.Count() == 1);
        }

        [TestMethod]
        public void Put_HavingInvalidKeyValue_ThrowsInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                table.Put(2, 5);
                table.Put(2, 3);
            });
        }

        [TestMethod]
        public void Remove_HavingValidKeyValue_DeleteFromTable()
        {
            table.Put(0, 5);

            table.Remove(0);

            Assert.IsTrue(table.Count() == 0);
        }

        [TestMethod]
        public void Remove_HavingInvalidKeyValue_ThrowsInvalidOperationException()
        {
            table.Put(0, 5);

            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                table.Remove(0);
                table.Remove(2);
            });
        }
    }
}
