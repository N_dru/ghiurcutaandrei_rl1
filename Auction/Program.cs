﻿using System;
using Auction.Service;

namespace Auction
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Auction!");

            var auctionPlaystation = new Service.Auction("PlayStation", 1500);
            var auction8kTV = new Service.Auction("8K Television", 2000);

            var bidder1 = new Bidder("Paul");
            var bidder2 = new Bidder("Peter");
            var bidder3 = new Bidder("Ted");

            auctionPlaystation.RegisterBidder(bidder1);
            auctionPlaystation.RegisterBidder(bidder2);

            auction8kTV.RegisterBidder(bidder1);
            auction8kTV.RegisterBidder(bidder2);
            auction8kTV.RegisterBidder(bidder3);

            auctionPlaystation.AddNewBid(bidder1,1650);
            auction8kTV.AddNewBid(bidder2,2100);
            auctionPlaystation.AddNewBid(bidder2,1750);
            auction8kTV.AddNewBid(bidder1, 2500);
            auctionPlaystation.AddNewBid(bidder1, 2000);
            auction8kTV.AddNewBid(bidder3, 2600);
            auction8kTV.AddNewBid(bidder1, 2700);
            auctionPlaystation.AddNewBid(bidder2, 3000);
            auction8kTV.AddNewBid(bidder2, 3000);
        }
    }
}
