﻿using System;

namespace Auction.Service
{
    public class Bidder{

        public string Name { get; }

        public Bidder(string name)
        {
            Name = name;
        }

        public void Update(Bidder highestBidder, string productName, int bidAmount)
        {
            if (highestBidder.Name == Name)
            {
                Console.WriteLine($"Hello {Name}. New bid of {bidAmount} placed by you on {productName}!");
            }
            else
            {
                Console.WriteLine($"Hello {Name}. New bid of {bidAmount} placed by {highestBidder.Name} on {productName}!");
            }
        }
    }
}
