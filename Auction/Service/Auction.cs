﻿using System;
using System.Collections.Generic;

namespace Auction.Service
{
    public class Auction
    {
        private readonly List<Bidder> bidders = new List<Bidder>();
        private readonly string productName;

        private Bidder highestBidder;
        private int highestBidAmount;

        public Auction(string productName, int startingAmount)
        {
            this.productName = productName;
            highestBidAmount = startingAmount;
        }

        public void AddNewBid(Bidder bidder, int bidAmount)
        {
            if (bidAmount > highestBidAmount)
            {
                highestBidAmount = bidAmount;
                highestBidder = bidder;
                NotifyBidders();
            }
        }

        public void NotifyBidders()
        {
            Console.WriteLine("-------------New bid placed-------------");
            foreach (var bidder in bidders)
            {
                bidder.Update(highestBidder, productName, highestBidAmount);
            }
        }

        public void RegisterBidder(Bidder bidder)
        {
            bidders.Add(bidder);
        }

        public void RemoveBidder(Bidder bidder)
        {
            bidders.Remove(bidder);
        }
    }
}
