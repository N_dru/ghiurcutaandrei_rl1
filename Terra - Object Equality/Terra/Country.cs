﻿using System;

namespace iQuest.Terra
{
    public class Country : IComparable
    {
        public string Name { get; }

        public string Capital { get; }

        public Country(string name, string capital)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Capital = capital ?? throw new ArgumentNullException(nameof(capital));
        }

        public override bool Equals(object obj)
        {
            Country country = obj as Country;
            return country != null && this.Name == country.Name && this.Capital == country.Capital;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            if (obj.GetType() != typeof(Country))
            {
                throw new ArgumentException("Objects are not the same type!");
            }

            Country country = (Country)obj;
            return this.Name.CompareTo(country.Name);
        }
    }
}