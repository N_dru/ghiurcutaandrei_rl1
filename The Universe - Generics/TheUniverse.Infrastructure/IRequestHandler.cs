﻿namespace iQuest.TheUniverse.Infrastructure
{
    public interface IRequestHandler<T>
    {
        object Execute(T request);
    }
}