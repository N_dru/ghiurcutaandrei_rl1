﻿using System.Collections.Generic;
using GrandCircus.View;

namespace GrandCircus.Model
{
    class Circus
    {
        private readonly List<IAnimal> animals = new List<IAnimal>();
        private readonly Arena arena;

        public Circus(Arena newArena)
        {
            this.arena = newArena;
            FillWithAnimals();
        }

        public void Perform()
        {
            arena.PresentCircus();
            foreach (IAnimal animal in animals)
            {
                arena.AnnounceAnimal(animal.Name, animal.SpeciesName);
                arena.DisplayAnimalPerformance(animal.MakeSound());
            }
        }

        private void FillWithAnimals()
        {
            IAnimal lion1 = new Lion("Simba");
            IAnimal snake1 = new Snake("Simina");
            IAnimal wolf = new Wolf("White Fang");
            IAnimal lion2 = new Lion("Mufasa");
            IAnimal snake2 = new Lion("Septimius");
            AddAnimal(lion1);
            AddAnimal(snake1);
            AddAnimal(wolf);
            AddAnimal(lion2);
            AddAnimal(snake2);
        }

        private void AddAnimal(IAnimal newAnimal)
        {
            bool Found = false;
            foreach (IAnimal animal in animals)
            {
                if(animal==newAnimal)
                {
                    Found = true;
                }
            }
            if(!Found)
            {
                animals.Add(newAnimal);
            }
        }
    }
}
