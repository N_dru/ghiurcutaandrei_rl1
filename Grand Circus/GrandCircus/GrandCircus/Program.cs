﻿using System;
using GrandCircus.Model;
using GrandCircus.View;

namespace GrandCircus
{
    class Program
    {
        static void Main(string[] args)
        {
            Arena arena = new Arena();
            Circus circus = new Circus(arena);
            circus.Perform();
        }
    }
}
