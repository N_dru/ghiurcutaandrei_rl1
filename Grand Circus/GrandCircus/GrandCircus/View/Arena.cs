﻿using System;
namespace GrandCircus.View
{
    class Arena
    {
        public Arena() { }
        public void PresentCircus()
        {
            Console.WriteLine("Welcome to the Grand Circus!!");
            Console.WriteLine("Let's begin!!");
        }

        public void AnnounceAnimal(string animalName,string animalSpecies)
        {
            Console.WriteLine($"We introduce you to {animalName} the {animalSpecies}!");
        }

        public void DisplayAnimalPerformance(string animalSound)
        {
            Console.WriteLine("Let's hear his shout:");
            Console.WriteLine(animalSound);
        }
    }
}
