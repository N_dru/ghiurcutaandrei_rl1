﻿namespace GrandCircus.Model
{
    public interface IAnimal
    {
        public string Name { get; }
        public string SpeciesName { get; }
        public string MakeSound();
    }
}
