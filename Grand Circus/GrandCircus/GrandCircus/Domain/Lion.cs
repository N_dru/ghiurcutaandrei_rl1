﻿namespace GrandCircus.Model
{
    class Lion : IAnimal
    {
        public string SpeciesName => "Lion";
        public Lion(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get;
        }

        public string MakeSound()
        {
            return "Rooaar!";
        }
    }
}
