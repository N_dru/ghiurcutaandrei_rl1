﻿namespace GrandCircus.Model
{
    public class Animal: IAnimal
    {
        public string Name
        {
            get;
        }

        public string SpeciesName
        {
            get;
        }

        public string MakeSound()
        {
            return "";
        }
    }
}
