﻿namespace GrandCircus.Model
{
    class Wolf : IAnimal
    {
        public string SpeciesName => "Wolf";
        public Wolf( string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get;
        }
        
        public string MakeSound()
        {
            return "OOOouuuuuuuuu!";
        }
    }
}
