﻿namespace GrandCircus.Model
{
    class Snake : IAnimal
    {
        public string SpeciesName => "Snake";
        public Snake(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get;
        }

        public string MakeSound()
        {
            return "SSSSsssSSSSssss!";
        }
    }
}
